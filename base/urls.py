from django.urls import path
from . import views

app_name = 'base'
urlpatterns = [
    path('node/<str:uuid>/', views.NodeView, name='node'),
    # path('local/<str:uuid>/', views.LocalView , name='local'),
    # path('map/', views.MapView , name='map'),
]
