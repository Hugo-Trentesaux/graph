from django.contrib import admin

# Register your models here.

from .models import Identity, Group, Follow, Membership

admin.site.register(Identity)
admin.site.register(Group)
admin.site.register(Follow)
admin.site.register(Membership)
