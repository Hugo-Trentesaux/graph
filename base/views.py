from django.shortcuts import render, get_object_or_404

from back.models import DBO

# Create your views here.

def NodeView(request, uuid):
    cNode = get_object_or_404(DBO, pk=uuid)

    try:
        uNode = request.user.authmethod.account.node()
    except: # e.g. anonymous user
        uNode = False

    if uNode == cNode or uNode == None:
        interact = False
    else:
        interact = uNode.actions(cNode)

    editable = cNode.isEditableBy(request.user)

    links = cNode.links.all()
    tags = cNode.tags.all()

    # TODO insert the following in a Node method
    edgesFrom = cNode.edgeFrom.all()

    rel = cNode.allRelatives()
    relatives = {}
    for kind, edgelist in rel.items():
        if edgelist:
            relatives.update({kind.__name__: edgelist})

    context = {'node': cNode, 'links': links, 'tags': tags, 'interact': interact, 'relatives': relatives,
    'uNode': uNode}
    return render(request, 'base/node.html', context)
