from django.db import models
from polymorphic.models import PolymorphicModel
from django.core.exceptions import ValidationError

from back.models import * # core node and edge models
from back.permissions import public
from back.utils import typename

from account.permissions import userRequired

# these models come to extend the base node and edge models
# additional information (image, links, tags, account) are stored in diferents database entries
# node and edge specifications inherit from base node and edge (id, group...)
# an other instance could implement different child types (paper, lab, researcher)

class Image(models.Model):
    """ base image class, simply an URL """
    imageURL = models.CharField(max_length=2048, null=True, blank=True)
    node = models.ForeignKey(Node, on_delete=models.CASCADE, related_name="image")

class Tag(models.Model):
    tag = models.CharField(max_length=64, unique=True)
    related = models.ManyToManyField(DBO, related_name="tags")
    def __str__(self):
        return self.tag

# =============================================

# LinkKinds = (
#     # external edge kinds (to external (non-graph) node kind resource)
#     ('website', 'Website'), # simple link to website, reciprocal: <a rel="me" href="https://graph.net/@h30x"></a>
#     ('email', 'Email'), # simple link to email, reciprocal: verify email address
#     ('address', 'Address'), # simple link to adress, reciprocal: address verified by post mail
#     ('mastodon', 'Mastodon'), # ...
#     ('diaspora', 'Diaspora'),
#     ('pixelfed', 'Pixelfed'),
#     ('facebook', 'Facebook'),
#     ('twitter', 'Twitter'),
#     ('instagram', 'Instagram'),
#     ('linkedIn', 'LinkedIn'),
#     # federated edge kinds (to external graph node)
#     ('unknown', 'Unknown'), # simple link to other node, edge kind not known (node kind or edge kind not implemented locally)
#     ('isonym', 'Isonym'), # symmetrical link between two id nodes on different instances
#         # add local types supported by distant instance (EdgeKind.Kinds)
# )


class Link(PolymorphicModel):
    anchor = models.ForeignKey(Node, on_delete=models.CASCADE, related_name="links")
    def __str__(self):
        return f"{typename(self)} on {self.anchor}"

class Website(Link):
    """ link to website """
    verified = models.BooleanField(default=False)
    url = models.CharField(max_length=256)
    def tryValidate(self):
        """ tries to validate the link by looking for <a rel="me" href="https://graph.net/id"></a>
        in the website and turns verified to on if so """
        pass
    def __str__(self):
        return super().__str__() + f": {self.url}"
    def fa(self): # font-awesome
        return "fa-globe"


# =============================================

# node kinds

@public
@userRequired
# TODO provide a decorator for name unique
class Identity(Node): # identity corresponding to a physical or moral person
    def nameExists(self):
        try:
            Identity.objects.get(name=self.name)
            return True
        except Identity.MultipleObjectsReturned:
            return True
        except Identity.DoesNotExist: # normal behavior
            return False
    def assertNameUnique(self):
        if self.nameExists():
            raise ValidationError("an identity with this name already exists")
    def creatable(self, **kwargs): # verifies if node is correct
        self.assertNameUnique()
        super().creatable(**kwargs)
    def editable(self, **kwargs): # verifies if node is correct
        self.assertNameUnique()
        super().editable(**kwargs)

@public
@userRequired
class Group(Node): # group of peoples / identities
    isOpen = models.BooleanField(default=True)
    def isEditableBy(self, account): # overwrites isEditableBy method
        if self.isOpen:
            return True
        else:
            return account.canEdit(self)
    # TODO add signal to add reciprocal link automatically if group is open

# return ({'kind': "member", 'message': {'ask': "ask to be member", 'asked': "membership requested", 'accept': "accept membership invitation", 'status': "member"}},

# edge kinds

@public
class Follow(Edge): # simple forward link (identity → node) attesting interest
    Symmetrical = False # no need of a symmetric link to be active (not needed since it's default)
    AllowedFromTo = { (Identity, Node) }
    Message = {
        'none': "follow",
        'forward': "following",
        'backward': "follow back",
        'both': "following",
    }

@public
class Membership(Edge): # group appartenance symmetrical link (node ←→ node:group)
    Symmetrical = True # needs a symmetric link to be considered as active
    AllowedFromTo = { (Node, Group), (Group, Node) }
    Message = {
        'none': "request membership",
        'forward': "membership request send",
        'backward': "accept membership request",
        'both': "member",
    }

# ============================================


# def isAllowed(Type, From, To):
#     return (From, To) in Type.AllowedFromTo # check is from/to pair is allowed
