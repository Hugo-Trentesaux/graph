from django.urls import path
from . import views

app_name = 'front'
urlpatterns = [
    path('graph/', views.GraphView, name='graph'),
    # path('map/<str:name>/', views.MapView , name='map'),
]
