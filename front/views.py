from django.shortcuts import render

# Create your views here.

def GraphView(request):
    return render(request, 'front/graph.html')
