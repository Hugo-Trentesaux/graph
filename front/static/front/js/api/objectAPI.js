function objectAPI(action, data, after) {

    url_prefix = getUrlPrefix(data);

    switch(action) {
        case 'GET':
            r = {
              method: 'GET',
              url: url_prefix + '/api/object/' + data.uuid + '/',
			  extract: extract,
            }
        break;
        case 'POST':
            r = {
              method: 'POST',
              url: url_prefix + '/api/object/' + 'create' + '/',
              data: data,
              headers: {'X-CSRFTOKEN': getCookie('csrftoken')},
			  extract: extract,
            }
        break;
        case 'PATCH':
            r = {
              method: 'PATCH',
              url: url_prefix + '/api/object/' + data.uuid + '/',
              data: data,
              headers: {'X-CSRFTOKEN': getCookie('csrftoken')},
			  extract: extract,
            }
        break;
        case 'DELETE':
            r = {
              method: 'DELETE',
              url: url_prefix + '/api/object/' + data.uuid + '/',
              headers: {'X-CSRFTOKEN': getCookie('csrftoken')},
			  extract: extract,
            }
        break;

        default:
            // error
    }

    m.request(r)
    .then(function (result) {after(result);})
    .catch(function (error) {}) // nothing because "Supplying an extract callback will prevent the promise rejection."
}

// if the prefix exists, remove it and return it without trailing slash, else return empty string
function getUrlPrefix(object) {
    if (typeof(object.prefix) !== "undefined") {
        prefix = object.prefix
        prefix.replace(/\/$/, "")
        delete(object.prefix)
        return prefix
    }
    else {
        return ''
    }
}

// replaces the stupid default extract function which ignores the status text (reason)
// created pull request to prevent skipping failure : https://github.com/MithrilJS/mithril.js/pull/2374
function extract(xhr, options) {
	var response = {
		json:xhr.responseText,
		status:xhr.status,
		statusText:xhr.statusText
	}
  try {response.json = response.json ? JSON.parse(response.json) : null}
	catch (e) {throw new Error("Invalid JSON: " + response.json)}
	return response
}
