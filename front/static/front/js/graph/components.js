// mount any function
function mountThis(sel, fun) {
  m.mount(document.querySelector(sel), {view:fun})
}

// graph info component
function displayGraphInfo() {
  if (typeof DATA.meta == 'undefined') {return m('p', "Loading graph info")}
  var milist = [
      m("li", [m("b", "number of nodes "), GRAPH.data.nodes.length]),
      m("li", [m("b", "number of links "), GRAPH.data.links.length])
  ]
  // to display what was sent by api
  for(var property in DATA.meta.info) {
     milist.push(m("li", [m("b", property + " "), DATA.meta.info[property]] ))
  }
  return [
    m("h3", "Graph info"),
    m("ul", milist),
  ]
}

// =============================================================================

function getOptionList(options) {
  oplist = []
  oplist.push(m("option[disabled][value=default]", "Select Type"))
  options.forEach(function(o) {oplist.push(m("option", {value: o}, o))})
  return oplist
}
// message block
function loadMessage() {
  return m("p", RESPONSE.message)
}
// unselect current node or edge
function unselect(type) {
  return function() {
    GRAPH.current[type] = null // reset current to null
    DATA.new[type] = null // reset new to null
    GRAPH.select(type, null) // redraw the graph
  }
}
function deleted(type) {
  return function() {
    GRAPH.current[type] = null // reset current to null
    DATA.new[type] = null // reset new to null
  }
}

// === node actions ===

// add node block (when not selected)
function addNodeBlock() {
  if(DATA.new.node == null) {
    DATA.new.node = {type: "default"}
  }
  return [
    m("label", m("b","Add node ")),
    m("button[type=button]", {onclick: function() {sendObject("create", "node", DATA.new.node)}}, "send"),
    m("br"),
    m("label", "node type"),
    m("select", {value: DATA.new.node.type, oninput: function(e) {DATA.new.node.type = e.target.value}}, [getOptionList(DATA.meta.nodeKinds)]),
    m("br"),
    m("label", "name"),
    m("input[type=text]", {oninput: function(e) {DATA.new.node.name = e.target.value}}),
  ]
}
// mixed edit block (when selected)
function dispEditDelNodeBlock() {
  if(DATA.new.node == null) {
    DATA.new.node = copy(GRAPH.current.node)
  }
  return [
      m("label", m("b", GRAPH.current.node.type + ": " + GRAPH.current.node.name + " ")),
      m("button[type=button]", {onclick: unselect("node")}, "unselect"),
      m("button[type=button]", {onclick: function() {sendObject("delete", "node", GRAPH.current.node, success=deleted("node"))}}, "delete"),
      m("br"),
      m("input[type=text]", {value: DATA.new.node.name, oninput: function(e) {DATA.new.node.name = e.target.value}}),
      m("button[type=button]", {onclick: function() {sendObject("update", "node", DATA.new.node)}}, "update"),
      m("br"),
      m("label", [m("b","uuid "), GRAPH.current.node.uuid]),
  ]
}
// links tho the two precedent
function nodeBlock() {
  if (GRAPH.current.node == null) {return addNodeBlock()}
  else {return dispEditDelNodeBlock()}
}

// =============================================================================
// === edge actions ===

// add edge block (if none selected)
function addEdgeBlock() {
  if(DATA.new.link == null) {
    DATA.new.link = {type: "default", source: {name:"undefined"}, target: {name:"undefined"} }
  }
  return [
    m("label", m("b", "Add edge ")),
    m("button[type=button]", {onclick: function() { sendObject("create", "link", DATA.new.link)}}, "send"),
    m("br"),
    m("label", "edge type"),
    m("select", {value: DATA.new.link.type, oninput: function(e) {DATA.new.link.type = e.target.value}}, [getOptionList(DATA.meta.edgeKinds)]),
    m("br"),
    m("label", "source: "),
    m("label", DATA.new.link.source.name),
    m("button", {onclick: function() {DATA.new.link.source = copy(GRAPH.current.node)}}, "capture"),
    m("br"),
    m("label", "target: "),
    m("label", DATA.new.link.target.name),
    m("button", {onclick: function() {DATA.new.link.target = copy(GRAPH.current.node)}}, "capture"),
    m("br"),
  ]
}
// edge info and delete block (if one selected)
function dispDelEdgeBlock() {
  if(DATA.new.link == null) {
    DATA.new.link = copy(GRAPH.current.link)
  }
  return [
        m("label", m("b", GRAPH.current.link.type + " ")),
        m("button[type=button]", {onclick: unselect("link")}, "unselect"),
        m("button[type=button]", {onclick: function() {sendObject("delete", "link", GRAPH.current.link, success=deleted("link"))}}, "delete"),
        m("br"),
        m("label", [m("b","from "), DATA.new.link.source.name + " "]),
        m("label", [m("b","to "), DATA.new.link.target.name]),
        m("br"),
        m("label", [m("b","uuid "), GRAPH.current.link.uuid]),
  ]
}
// links to the two precedent function
function edgeBlock() {
  if (GRAPH.current.link == null) {return addEdgeBlock()}
  else {return dispDelEdgeBlock()}
}






// switch buttons
// alternative: use m.trust
// other alternative: extend mithril http://lhorie.github.io/mithril-blog/extending-the-view-language.html
// var button = m("label.switch", [
//       m("input[type='checkbox']"),
//       m("span.slider.round"),
//       ]);
// var Component = {
//   view: function() {
//     return [
//       m("h3", "Possible edges"),
//       m("ul", [
//           m("li", ["follow", button]),
//           m("li", ["enter", button]),
//           m("li", ["like", button]),
//         ]),
//     ]
//   }
// }
// m.mount(root, Component)
