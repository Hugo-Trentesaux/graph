// these function are here to find good parameters
function getLinkDistance(data) {
    // return 0
    return 500 / Math.pow( data.links.length, 0.2 )
}
function getNodeRadius(data) {
    return 30 / Math.pow( data.nodes.length, 0.333)
}
function getCharge(data) {
    // return -10000
    return -100 / Math.pow( data.nodes.length, 0.8)
}
function getStrokeWidth(data) {
    return 10 / (1 + Math.log10( data.links.length ))
}
function getStrokeOpacity(data) {
    return 2 / (1 + 3*Math.log10( data.links.length ))
}

function completeParam(graph) {
    graph.param.charge = getCharge(graph.data)
    graph.param.nodeRadius = getNodeRadius(graph.data)
    graph.param.strokeWidth = getStrokeWidth(graph.data)
    graph.param.linkDistance = getLinkDistance(graph.data)
    graph.param.strokeOpacity = getStrokeOpacity(graph.data)
}

// =============================================================================
// =============================================================================
// =============================================================================

function graph(
    data={nodes: [], links: []},
) {
    // make this object available for other functions
    self = this

    this.root = d3.select("#graph-box")
    this.data = data
    this.layout = {}
    this.mouse = {}
    this.current = {node: null, link: null}
    this.new = {node: null, link: null}
    this.param = {
        width: 512,
        height: 512,
    }

    // =========================================================================
    //                                                                      INIT
    // initialize DOM elements and force render elements
    this.initComponents = function() {
        // svg is where graph will be rendered
        this.layout.svg = this.root
            .append("div")
                .attr("id", "graph")
            .append("svg:svg")
                .attr("width", this.param.width)
                .attr("height", this.param.height)
                .attr("pointer-events", "all");

        // background
        this.layout.svg.append('svg:rect')
                .attr('width', this.param.width)
                .attr('height', this.param.height)
                .attr('class', 'graph-background')
                .attr('fill', 'white');

        // define arrow markers for graph links
        this.layout.svg.append('svg:defs').append('svg:marker')
                .attr('id', 'arrow')
                .attr('viewBox', '0 -5 10 10')
                .attr('refX', this.param.nodeRadius)
                .attr('markerWidth', this.param.nodeRadius/7)
                .attr('markerHeight', this.param.nodeRadius/7)
                .attr('orient', 'auto')
              .append('svg:path')
                .attr('d', 'M0,-5L10,0L0,5')
                .attr('fill', '#000');

        // graphLayer is the svg group where the nodes and edges will live
        this.layout.graph = this.layout.svg
            .append('svg:g')
                //.on(...)

        // a svg group for links
        this.layout.links = this.layout.graph
            .append("g")
                .attr("stroke", "#999")
                .attr("stroke-opacity", 0.6)
        this.link = this.layout.links.selectAll("line")

        // a svg group for nodes
        this.layout.nodes = this.layout.graph
            .append("g")
                .attr("stroke", "#fff")
                .attr("stroke-width", 1.5)
        this.node = this.layout.nodes.selectAll("circle")

        // zoom behavior
        var zoom_handler = d3.zoom().on("zoom", zoom_actions);
        function zoom_actions(){ self.layout.graph.attr("transform", d3.event.transform); }
        zoom_handler(this.layout.svg);

        // force is the object responsible of computing node position
        this.force = d3.forceSimulation(this.data.nodes)

        this.forceLink = d3.forceLink(this.data.links)
            .distance(this.param.linkDistance)
                //.id(d => d.uuid)

        this.force
            .force("link", this.forceLink)

        this.force
            .force("charge",
                d3.forceManyBody()
                    .strength(this.param.charge)
                )
            .force("center",
                d3.forceCenter(this.param.width / 2, this.param.height / 2)
            )
            .on("tick", () => this.tick())
    }

    // =========================================================================
    //                                                                    REDRAW
    // redraw the graph (if an element was added or deleted)
    this.redraw = function() {
        // ======================================================== NODES
        // select all circles and join data
        this.node = this.layout.nodes
            .selectAll("circle")
            .data(this.data.nodes, d => d.uuid); // this.node has .exit and .enter functions

        // for leaving nodes
        this.node
            .exit().transition().attr("r", 0).remove();

        this.node = this.node // updates this.node with new nodes
            .enter()
                .append("circle")
                    .attr("r", this.param.nodeRadius)
                    .attr("class", "node")
                    .on("mousedown", nodemousedown )
                    .on("mouseup", nodemouseup )
            .merge(this.node)
            .classed("node_selected", d => d === self.current.node);

        this.node
            .append("title")
            .text(d => d.name)

        // ======================================================= LINKS
        // select all lines and join data
        this.link = this.layout.links
            .selectAll("line")
            .data(this.data.links, d => d.uuid);

        // for leaving links
        this.link
            .exit().remove();

        this.link = this.link // updates this.link with new links
            .enter()
                .append("line")
                    .attr("stroke-width", this.param.strokeWidth)
                    .attr("stroke-opacity", this.param.strokeOpacity)
                    .attr("class", "link")
                    .style('marker-end', 'url(#arrow)')
                    .on("mousedown", linkmousedown )
            .merge(this.link)
            .classed("link_selected", d => d === self.current.link);
    }

    // =========================================================== TICK
    // run when force updates
    this.tick = function() {
        this.link
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);

        this.node
            .attr("cx", d => d.x)
            .attr("cy", d => d.y);
    }
    // =========================================================================
    //                                                                      EDIT
    this.removeNode = function(node) {
        this.removeLinkedEdges(node)
        this.data.nodes.splice(this.data.nodes.indexOf(node), 1);
    }
    this.removeEdge = function(edge) {
        this.data.links.splice(this.data.links.indexOf(edge), 1);}
    this.removeLinkedEdges = function(node) {
        // select linked edges
        toSplice = this.data.links.filter( l => (l.source === node) || (l.target === node) );
        // remove these edges
        toSplice.map(l => this.removeEdge(l))
    }
    this.getNodeByUUID = function(uuid) {
        for (var i = this.data.nodes.length - 1; i >= 0; --i) {
            if (this.data.nodes[i].uuid == uuid) { return this.data.nodes[i] }
        }
    }
    this.getEdgeByUUID = function(uuid) {
        for (var i = this.data.links.length - 1; i >= 0; --i) {
            if (this.data.links[i].uuid == uuid) { return this.data.links[i] } }
    }
    this.removeNodeByUUID = function(uuid) {
        this.removeNode(this.getNodeByUUID(uuid)) }
    this.removeEdgeByUUID = function(uuid) {
        this.removeEdge(this.getEdgeByUUID(uuid)) }

    this.select = function(type, obj) {
        this.current[type] = obj
        this.redraw()
    }

    // =========================================================================
    //                                                              MOUSE EVENTS
    resetMouseVars = function() {
        self.mouse.down_node = null;
        self.mouse.up_node = null;
        self.mouse.down_link = null;
    }

    nodemousedown = function(d) {
        // disable zoom
        // self.layout.graph.call(d3.behavior.zoom().on("zoom"), null);
        self.mouse.down_node = d;
        if (self.mouse.down_node == self.current.node) self.current.node = null;
        else self.current.node = self.mouse.down_node;
        // selected_link = null;
        currentChanged("node")
        self.redraw();
      }

    nodemouseup = function(d) {
        if (self.mouse.down_node) {
        self.mouse.up_node = d;
        if (self.mouse.up_node == self.mouse.down_node) { resetMouseVars(); return; }
        // enable zoom
        // self.layout.graph.call(d3.behavior.zoom().on("zoom"), rescale);
        self.redraw();
        }
    }

    linkmousedown = function(d) {
        self.mouse.down_link = d;
        if (self.mouse.down_link == self.current.link) self.current.link = null;
        else self.current.link = self.mouse.down_link;
        // self.selected_node = null;
        currentChanged("link")
        self.redraw();
      }

    // =========================================================================
    //                                                                 DEBUGGING
    // fills this.data with default data (test purpose)
    this.debug = function() {
        this.data.nodes.push({x:0,y:0})
        this.data.nodes.push({x:1,y:0})
        this.data.nodes.push({x:1,y:1})
        this.data.nodes.push({x:0,y:1})

        this.data.links.push({source:0,target:1})
        this.data.links.push({source:2,target:1})
        this.data.links.push({source:3,target:1})
        this.data.links.push({source:3,target:2})

        completeParam(this)

        this.initComponents()
        this.redraw()
    }

    this.init = function() {
        completeParam(this)
        this.initComponents()
        this.redraw()
    }

    this.init()

}
