// copy object
function copy(obj) {
  return JSON.parse(JSON.stringify(obj))
}

// load properties in given object (it modifies the object)
// updates a based on b values
function update_dic(a, b) {
  	for(key in b){
  		a[key] = b[key]
      }
}

// get cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// build url based on uuid and extra parameters
function buildUrl(uuid, extra) {
  var path = "/api/object/" + uuid + "/"
  return typeof extra == "undefined" ? path : extra.url + path
}

// get distant value and put the result in local data
var getObject = function(type, uuid) {
  data = {uuid:uuid}
  success = function(result) {
    DATA.new[type] = null // if we change selection, current being edited is set to null
    update_dic(GRAPH.current[type], result.json)
    m.redraw()
  }
  objectAPI('GET', data, success, undefined)
}

// get distant graph and put the result in data call only at the beginning
var getGraph = function() {
  m.request({
    method: "GET",
    url: "http://localhost:8000/api/graph/",
  })
  .then( function(result) {
    // DATA.graph = result
    // displayGraph()
    DEBUG = result
    var data = {nodes:DEBUG.nodes, links:DEBUG.edges}
    GRAPH = new graph(data=data)
        mountThis("#message", loadMessage)
        mountThis("#nodeBlock", nodeBlock)
        mountThis("#edgeBlock", edgeBlock)
        mountThis("#graphInfo", displayGraphInfo)
  })
}

// get metadata
var getMetaData = function() {
  m.request({
    method: "GET",
    url: "http://localhost:8000/api/meta/",
  })
  .then( function(result) {
    DATA.meta = result
  })
}

// update local copy of graph
// prevents loading whole graph if it is huge
// helps smooth rendering when updating graph
function updateLocal(action, type, data, returned=undefined) {
    switch(type) {
      case "node":
        switch(action) {
          case "create":
            returned.x = 0
            returned.y = 0
            returned.vx = 0
            returned.vy = 0
            GRAPH.data.nodes.push(returned)
            GRAPH.force.nodes(GRAPH.data.nodes) // informs of the new node
						GRAPH.current.node = returned
          break;
          case "update":
						n = GRAPH.getNodeByUUID(returned.uuid)
						n.name = returned.name // TODO the rest
          break;
          case "delete":
            GRAPH.removeNodeByUUID(data.uuid)
          break;
          default:
        }
      break

      case "link":
      switch(action) {
        case "create":
          newlink = copy(returned)
          newlink.source = GRAPH.getNodeByUUID(newlink.source.uuid) // binds source to existing node
          newlink.target = GRAPH.getNodeByUUID(newlink.target.uuid) // binds target to existing node
          GRAPH.data.links.push(newlink)
          GRAPH.forceLink.links(GRAPH.data.links) // informs of the new link
					GRAPH.current.link = newlink
        break;
        case "update":
          // not implemented
        break;
        case "delete":
          GRAPH.removeEdgeByUUID(data.uuid)
        break;
        default:
      }
      break
    }
}
// send object (action can be create, update, or delete)
var sendObject = function(action, type, data, success=undefined) {
  after = function(result) {
    var succeeded = (result.status >= 200 && result.status < 300)
    if (succeeded) {
      RESPONSE = result.json
      RESPONSE.message = result.statusText
      if (success) {success();}
      updateLocal(action, type, data, result.json)
      GRAPH.force.alpha(1)
      GRAPH.force.restart()
      GRAPH.redraw()
    }
    else {
      RESPONSE = result.statusText // that's all
    }
  }
  if (action == 'create') {
    objectAPI('POST', data, after)
  }
  else if (action == 'update') {
    objectAPI('PATCH', data, after)
  }
  else if (action == 'delete') {
    objectAPI('DELETE', data, after)
  }
}

// function to call when current node or edge changes
function currentChanged(type) {
  // if node or edge is not already fetched
  if(GRAPH.current[type] != null && typeof GRAPH.current[type].type == "undefined") {
    getObject(type, GRAPH.current[type].uuid)
  }
  // no need to retreive data, just redraw with new local copy
  else {
    DATA.new[type] = null // if we change selection, current being edited is set to null
    m.redraw();
  }
}
