## Install graph locally

Create and activate a python3 virtual environnement.

    virtualenv env -p python3
    source env/bin/activate

Download the project and install requirements.

    https://gitlab.com/Hugo-Trentesaux/graph.git
    cd graph
    pip install -r requirements.txt

Run the server locally.

    python manage.py runserver

Access it in your browser ath the address http://localhost:8000/

## Edit graph

Now, the only way to edit the graph is by using the custom interface. Create a superuser account on your local db :

    python manage.py createsuperuser

Log in http://localhost:8000/admin/ and visit http://localhost:8000/view/graph/.
