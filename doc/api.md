# API

The very first versions had a folkloric API. I understood quickly that this part of the program was critical since it shapes all the server side parsing and the client side object representation. So here are the API specification :

## JSON serialization

The API uses JSON as serialization format. Client-side object representation can thus be different from server-side models.

Here is how the database objects are represented in JSON.

- **DBO** (database object): {type: `type`, uuid: `uuid`}
  - **node** {type: `type`, uuid: `uuid`, name: `name`}
    - ...
  - **edge** {type: `type`, uuid: `uuid`, source: `uuid`, target: `uuid`}
    - ...

When inheriting from nodes and edges, fields are just added to the serializer. Also allows nested objects... For a federated edge, `source` or `target` can be replaced by the full URL, if the node is distant (inherits from `Fedge`). 

## URL's

| HTTP     | URL                   | action                           | argument | result |
| -------- | --------------------- | -------------------------------- | -------- | ------ |
| `GET`    | `/api/object/{uuid}`  | get JSON serialized object       | /        | {*}    |
| `POST`   | `/api/object/create/` | create object based on JSON data | {°}      | {*}    |
| `PATCH`  | `/api/object/{uuid}`  | updates partially the object     | {°}      | {*}    |
| `DELETE` | `/api/object/{uuid}`  | deletes the object               | /        | -      |

**argument and results**

- `{*}` complete serialized object
- `{°}` partial serialized object (no uuid, optionnal fields for patch)
- `/` no argument
- `-` only response code

**response code** mean what they usually mean and are explained in the message.

## Whole graph cache

`GET /api/cache/graph` returns the whole graph in the form (JSON) :

```JSON
{"nodes": [
    {"name": "Alice", "uuid": "1188510074254b1ca886e09208e2225b", "x": 0.0, "y": 1.0},
    {"name": "Bob", "uuid": "daefc631f38b4bf8aa8b58e42e329c08", "x": 1.0, "y": 0.0}
],
 "edges": [
     {"uuid": "6f36b8fcb6c34d87831bab91973c7fbc", "source": 1, "target": 0},
     {"uuid": "347d2afefa89476490085e6fa859191e", "source": 0, "target": 1}
 ]}
```

The goal is to provide a quick view of the interesting parts of the graph (the meaningful nodes and edges) with predefined initial locations for nodes in order to simplify the visualization.

Keep in mind that for huge number of nodes and edges, making the query and computing the position of the nodes can be long. That's why a cache is recommended to access the whole graph.

## General graph information

Some information about the graph can be useful when building the interface.

`GET /api/meta/tree` gives you a tree of the implemented types where each leaf is a pair `[object, []]` and each branch is a pair `[object, [children]]`.

This should at least look like :

```json
[DBO, [Node, []], [Edge, []]]
```

TODO: define object representation (e.g. `{type: "Node", public: false}`).

Then, to get the list of public Node and Edge kinds, we only have to unnest the children list of Node or Edge and keep only the public ones. 

`GET /api/meta/version` gives you information about the program version (could be useful to monitor code propagation and API compatibility).



# Client

I will develop modules to allow communicate to the API through a client program.

## JavaScript

I implemented a JavaScript module to talk with the API. Here is how it works :

`objectAPI(action, data, success, failure)`

`action` is the desired action (`GET`, `POST`, `PATCH`, `DELETE`) in full letters. I choose this to make the implementation transparent.

`data` is a javascript object that contains :

- GET or DELETE: whatever the user wants as long as it contains at least a `uuid` field with the uuid as a string.
- POST or PATCH: exactly what is defined in the API

it can optionally contain an `prefix` field that will be used to prefix the API address (e.g. `https://external.node.org/` before `/api/object/[...]`) and removed from the arguments.

`success` and `failure`, are the functions executed after the promise is resolved, taking the result of the request as first argument.

This function calls `getCookie('csrftoken')` which should be implemented and return a csrftoken before I change the authentication method.