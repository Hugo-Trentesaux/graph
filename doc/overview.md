# Overview

## overall functioning

### Objects (back)

All the edges and nodes in a graph inherit from a single parent class called DBO (database object) identified by an UUID. Node and Edge are base models modeling the whole graph. They look roughly like this:

```python
class DBO(PolymorphicModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

class Node(DBO):
    name = models.CharField(max_length=32)
    relTo = models.ManyToManyField('self', symmetrical=False, through='Edge', related_name="relFrom")

class Edge(DBO):
    fromNode = models.ForeignKey('Node', related_name="edgeFrom", on_delete=models.CASCADE)
    toNode = models.ForeignKey('Node', related_name="edgeTo", on_delete=models.CASCADE)
```

These base class provide general methods that will apply to any child class. At the end, to develop a module, one can inherit from these models and benefit from all the existing methods and generic interfaces.

### API (back)

#### GET

The API serves the objects by uuid:

    GET http://localhost:8000/api/object/529493cbc3974b64a81f53a5ec8d8cb1/
    {"type": "Identity", "name": "A", "uuid": "529493cbc3974b64a81f53a5ec8d8cb1"}

and returns an object with asked uuid or 404 if not found.

It also serves the whole graph cache (all nodes and edges) with initial (x,y) position:

    GET http://localhost:8000/api/graph/
    {"nodes": [
        {"name": "A", "uuid": "529493cbc3974b64a81f53a5ec8d8cb1", "x": -0.4084039952597164, "y": 0.707376385376044},
        {"name": "B", "uuid": "cf7cf053941b40628d4517fcc954b879", "x": 1.0, "y": 0.10576605305833524},
        {"name": "C", "uuid": "eacb92b126dc4be1b793ce463b0e94ab", "x": -0.5915960047402837, "y": -0.8131424384343793}
        ],
    "edges": [
        {"uuid": "f6c2be242aa14d1585e4e132cf03772c", "source": 1, "target": 0},
        {"uuid": "9874e4b07841442da4df67986f16ad1d", "source": 2, "target": 0},
        {"uuid": "7ae366c27a2448f495a5f9a994c605cb", "source": 2, "target": 1},
        {"uuid": "126c69d78d274b41a18a1563f5207acb", "source": 0, "target": 1},
        {"uuid": "3f28dfa1b056474eb7f96fb34c68b791", "source": 0, "target": 2}
        ]
    }

#### POST

The API accepts POST request to add, update, or delete objects (not a REST API).
Urls are:

    /api/create/
    /api/update/
    /api/delete/

The server performs the following check/tasks:

**create**

1. a type is given
2. the given type corresponds to a public class
3. create a new instance of the class by populating with given arguments (e.g. "name" for node)
4. update the graph cache
5. return the new object as JSON along with success message

**update**

1. steps 1. and 2. of "create" (yes it's useless)
2. get instance of given type with given uuid
3. update the instance with arguments provided
4. return the updated object as JSON along with success message

**delete**

1. steps 1. and 2. of update
2. delete the object
3. update the graph cache
4. return success message

If an error occurs, it will return an error code (400, 403, 404) with an error message.

### Cache

I designed the cache because I tried to work with a big database (1500 nodes, 15000 edges) and the query was a bit long to perform. The cache allows to retreive the data instantly. I also added a basic server-side force layout algorithm to provide good initial position for nodes and avoid computing this client-side.

If the cache fails updating (not very stable at the moment), please do a new graph dump from python-django shell:

    >>> newGraphDump()

The cache is only meant to serve the whole graph at a time if it is too big.

## Federation

The graph project is meant to have a decentralized architecture with each instance implementing it's own types (possibly different from other). For this purpose I created the abstract class Fnode (federated node), the class Fedge (federated edge) and the classes Outgoing and Ingoing inheriting from Fedge. Fedge will store ingoing and outgoing links with on one side a Node and on the other and URL. The url will be stored in a Fnode created on the fly when trying to access the distant node. The client will fetch himself the node information from the distant graph instance according to the URL.

We can also imagine several instances implementing the same types and federated through activitypub...
