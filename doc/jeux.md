# Jeux

Pour que le cœur du programme graph reste aussi général que possible, il est important de diversifier ses applications. Le jeu est une manière ludique de développer et d'utiliser un outil. C'est d'ailleurs un des secteurs qui fait le plus évoluer l'informatique tant à bas niveau (cartes graphiques, moteurs de rendu) qu'à haut niveau (interfaces graphiques).

De plus, certains jeux sont par nature modélisables par un graphe. Par exemple :

- little alchemy
  - nœuds : éléments, combinaisons
  - liens : mélange, produit
- les livres jeu
  - nœuds : pages, choix, événement
  - liens : choix disponibles, conséquence du choix
- achikaps (jeu smartphone sur l'évolution d'un graphe en temps réel)

En modélisant un jeu sur le projet *graph*, le jeu devient fondamentalement interactif : co-écriture du livre jeu, ajout d'éléments et de combinaisons dans little alchemy...

Il bénéficiera aussi des outils généraux d'un graphe (calcul du coefficient de clustering, connectivité...)   ce qui permettra d'analyser son développement.