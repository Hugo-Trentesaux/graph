# Monnaie libre

**Objectifs du module "wot"**

- services périphériques à la toile de confiance :
  - visualisation des promesses de certification
  - intégration des informations de Césium+
- synchronisation avec un nœud duniter
  - actualisation de la base de données automatique 



**Autres modules à développer pour la monnaie libre**

- gestion des structures de communication autour de la ML
  - sites web
  - associations
  - kit de communication (autocollants...)
- transparence des systèmes
  - rémunération des développeurs de duniter