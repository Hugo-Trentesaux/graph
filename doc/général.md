# Graph

Améliorations générales pour le projet Graph.

**Module base**

Le but du module *base* est d'offrir un certain nombre de fonctionnalités habituellement présentes dans les applications.

- tags
- lien sociaux
- moteur de recherche

Il doit aussi permettre l'authentification par un service extérieur.

**Module account**

Le module *account* offre un système d'authentification et de compte qui gère les permissions.

**Compatibilité**

Le projet graph doit se rendre compatible avec d'autres outils de calcul sur les graphes, et d'autres formats de données.

**formats**

- JSON
- CSV
- have a look at SPARQL, RDF, OWL

**bibliothèques de graphe en python**

- https://igraph.org/
- https://networkx.github.io/
- https://graph-tool.skewed.de/

**logiciels lourds avec GUI**

- https://gephi.org/
- https://graphviz.org/
- https://cytoscape.org/

**bibliothèques de visualisation**

- https://www.kenedict.com/interactive-visualisation/
- http://visjs.org/
- https://vega.github.io/vega/
- http://sigmajs.org/

à regarder :

- http://linkeddata.org/
- https://wiki.dbpedia.org/
- https://lod-cloud.net/
- https://data.worldbank.org/
- http://linkedgeodata.org/About
- https://www.w3.org/DesignIssues/LinkedData.html
- http://wifo5-03.informatik.uni-mannheim.de/bizer/pub/LinkedDataTutorial/
- http://www.lespetitescases.net/ou-s-amuser-avec-sparql


à regarder aussi :

- http://graphstream-project.org/doc/Tutorials/Graph-Visualisation/
- http://graphalchemist.github.io/Alchemy/#/
- https://linkurio.us/
- https://www.caida.org/tools/visualization/walrus/
- https://datavizcatalogue.com/
- https://gigraph.io/
- https://rawgraphs.io/
- http://www.biofabric.org/
- http://tulip.labri.fr/TulipDrupal/
- https://www.yworks.com/products/yed
- http://lanet-vi.fi.uba.ar/
