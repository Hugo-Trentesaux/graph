import itertools

def UUIDtoStr(uuid):
    return str(uuid).replace('-', '')

def typename(obj):
    return type(obj).__name__

def all_subclasses(cls):
    return set(cls.__subclasses__()).union(
        [s for c in cls.__subclasses__() for s in all_subclasses(c)])

def all_public_subclasses(cls):
    # clsSet = all_subclasses(cls)
    # return set().union([c for c in clsSet if c.isPublic ])
    return filter(lambda c: c.isPublic, all_subclasses(cls))

def publicKindNames(cls): # TODO remove
    return set().union([cls.__name__ for cls in all_public_subclasses(cls)])

def publicKindDict(cls): # TODO remove
    return {cls.__name__: cls for cls in all_public_subclasses(cls)}

def classDict(cls, fn=lambda x:x): # creates a dict of all subclasses of cls with fn(cls) as key
    return {fn(cls): cls for cls in {cls}.union(all_subclasses(cls)) }

def schemaDict(cls): # designed for DBOSchema
    return classDict(cls, fn=lambda x:x._type)

def classTree(cls): # return all subclasses in form of a tree (nested list)
    return [cls, [[b for c in cls.__subclasses__() for b in classTree(c)]]]

def linearMRO(cls, root): # returns the list of parent from cls to root (to avoid getting PolymorphicModel aso)
    # return list(itertools.takewhile(lambda c: c != root, cls.__mro__))
    return [c for c in cls.__mro__[:cls.__mro__.index(root)+1]]

def firstKeyIn(klist, dic):
    for k in klist:
        if k in dic:
            return dic[k]
    raise Exception(f"none of {klist} is in {dic.keys()}")
