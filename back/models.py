from django.db import models
from django.urls import reverse
from polymorphic.models import PolymorphicModel
from django.core.exceptions import ValidationError

from .permissions import Permission, notEmptyName
from .utils import UUIDtoStr, typename, all_public_subclasses
import uuid

# core model classes containing only node and edge without any related information
# Fedge models implement simulated edge for federation purpose


class GraphDump(models.Model):
    """ class for graph dump in JSON format """
    created = models.DateTimeField(auto_now_add=True)
    jsonData = models.TextField()

class DBO(PolymorphicModel, Permission):
    """ base class for database object (edge, node...) """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    isPublic = False # True if available to end user, False if not
    # TODO add creation date, modification date... meta info → in separate object ?, in heritable object ?

@notEmptyName
class Node(DBO):
    """ represents a node of the graph. """
    name = models.CharField(max_length=32) # is it really necessary ?
    relTo = models.ManyToManyField('self', symmetrical=False, through='Edge', related_name="relFrom") # directional link to other node using edge
    def get_absolute_url(self):
        return reverse('node', args=[self.pk])
    @property
    def url(self):
        return self.get_absolute_url()
    def __str__(self):
        return self.name
    def asDict(self):
        return {'type': typename(self), 'name': self.name, 'uuid': UUIDtoStr(self.pk)}
    def neighborhood(self, edgeKinds=[None], nodeKinds=[None], backwards=False):
        """ returns the immediate neighborhood (step 1) of the node:
        edges must be one of edgekinds
        nodes must be one of nodekinds
        if backwards is set to true, also returns nodes linked through backward edges """
        # TODO find a better way to set default to yet undefined class
        if edgeKinds == [None]:
            edgeKinds = [Edge]
        if nodeKinds == [None]:
            nodeKinds = [Node]
        nodes = set() # set do not allow duplicate value
        edges = set()
        nodes.add(self) # a node is in its neighborhood
        tmp = self.edgeFrom.instance_of(*edgeKinds) # select edges of given kinds
        tmp = [(e, e.toNode) for e in tmp if isinstance(e.toNode, tuple(nodeKinds))] # filter on linked node type
        if tmp:
            tmpe, tmpn = zip(*tmp) # get edges and nodes
            nodes.update(set(tmpn))
            edges.update(set(tmpe))
        if backwards: # if backwards is true, also search for backward links
            tmp = self.edgeTo.instance_of(*edgeKinds) # select edges of given kinds
            tmp = [(e, e.fromNode) for e in tmp if isinstance(e.fromNode, tuple(nodeKinds))] # filter on linked node type
            if tmp:
                tmpe, tmpn = zip(*tmp) # get edges and nodes
                nodes.update(set(tmpn))
                edges.update(set(tmpe))
        return {'nodes': nodes, 'edges': edges}
    def explore(self, depth, edgeKinds=[None], nodeKinds=[None], backpropagate=False):
        """ returns a list with in position k the new nodes and edges after k steps of exploration """
        # TODO find a better way to set default to yet undefined class
        if edgeKinds == [None]:
            edgeKinds = [Edge]
        if nodeKinds == [None]:
            nodeKinds = [Node]
        graph = [{'nodes':set(), 'edges': set()} for _ in range(depth+1) ] # initialize graph to empty output
        graph[0]['nodes'].add(self) # add self to step zero
        allNodes = set()
        allEdges = set()
        for k in range(depth): # for each step
            for node in graph[k]['nodes']: # for each new node for precedent step
                neighborhood = self.neighborhood(edgeKinds=edgeKinds, nodeKinds=nodeKinds, backwards=backpropagate)
                newNodes = neighborhood['nodes'] - allNodes
                newEdges = neighborhood['edges'] - allEdges
                graph[k+1]['nodes'].update(newNodes)
                graph[k+1]['edges'].update(newEdges)
                allNodes.update(newNodes)
                allEdges.update(newEdges)
        return graph
    def relatives(self, cls):
        """ returns a list of the nodes linked through the given edge class
        if class is Symmetrical, returns only nodes linked through symmetrical relation """
        rel = self.edgeFrom.instance_of(cls)
        if cls.Symmetrical: # need to verify if relation is symmetric
            rel = [r for r in rel if r.isSymmetric()]
        return rel
    def allRelatives(self):
        """ returns a dict with edge kinds as keys and relatives for this edge kind as values """
        rel = {}
        for cls in Edge.allPublicSubclasses():
            rel.update({cls: self.relatives(cls)})
        return rel
    def actions(self, node):
        """ returns a list of possible actions that self could do with node """
        return [cls.action(self, node) for cls in Edge.allPossiblePublicSubclasses(type(self), type(node))]

class Edge(DBO):
    """ represents an edge of the graph """
    Symmetrical = False # by default, and edge does not need to be symmetric to be recognised as existing
    # symmetric edges have the "request/accept" behavior
    fromNode = models.ForeignKey('Node', related_name="edgeFrom", on_delete=models.CASCADE)
    toNode = models.ForeignKey('Node', related_name="edgeTo", on_delete=models.CASCADE)
    def get_absolute_url(self):
        return reverse('edge', args=[self.pk])
    @property
    def url(self):
        return self.get_absolute_url()
    def __str__(self):
        return "{} -> {}".format(self.fromNode, self.toNode)
    def asDict(self):
        return {'type': typename(self), 'uuid': UUIDtoStr(self.pk), 'source': self.fromNode.asDict(), 'target': self.toNode.asDict()}
    @classmethod
    def exists(cls, fromNode, toNode):
        try:
            Edge.objects.instance_of(cls).get(fromNode=fromNode, toNode=toNode)
            return True
        except Edge.DoesNotExist:
            return False
    def alreadyExists(self):
        return type(self).exists(self.fromNode, self.toNode)
    def isSymmetric(self):
        try:
            Edge.objects.instance_of(type(self)).get(fromNode=self.toNode, toNode=self.fromNode) # look for similar type in opposite direction
            return True
        except Edge.DoesNotExist:
            return False
    @classmethod
    def allowed(cls, fromNodeType, toNodeType):
        pair = (fromNodeType, toNodeType)
        for possiblepair in cls.AllowedFromTo:
            if issubclass(pair[0], possiblepair[0]) and issubclass(pair[1], possiblepair[1]):
                return True
                break
        return False
    def isAllowed(self): # return True if edge is listed in allowed pairs (AllowedFromTo)
        return type(self).allowed(type(self.fromNode), type(self.toNode))
    def creatable(self, **kwargs): # verifies if edge is correct
        if not self.isAllowed(): # check if pair is allowed
            raise ValidationError(f"{typename(self.fromNode)} -> {typename(self.toNode)} not allowed for {typename(self)}")
        if self.alreadyExists(): # check if edge already exists
            raise ValidationError("this edge already exists")
        else:
            super().creatable(**kwargs)
    @classmethod
    def message(cls, fromNode, toNode): # get message from node to node
        forward = cls.exists(fromNode, toNode)
        backward = cls.exists(toNode, fromNode)
        if not forward and not backward:
            return cls.Message['none']
        if forward and not backward:
            return cls.Message['forward']
        if not forward and backward:
            return cls.Message['backward']
        if forward and backward:
            return cls.Message['both']
        raise Exception("WAT?")
    @classmethod
    def allPublicSubclasses(cls):
        return all_public_subclasses(cls)
    @classmethod
    def allPossiblePublicSubclasses(cls, sourceType, targetType):
        """ returns a set of public edge subclasses available for this directed node pair """
        return set().union([cls for cls in Edge.allPublicSubclasses() if cls.allowed(sourceType, targetType)])
    @classmethod
    def action(cls, source, target):
        return Action(cls, source, target)

class Action():
    """ class responsible for providing information about edge management """
    def __init__(self, cls, source, target):
        self.cls = cls
        self.source = source
        self.target = target
        self.message = cls.message(source, target)
        self.forward = cls.exists(source, target)
        self.backward = cls.exists(target, source)
    @property
    def name(self):
        if self.forward:
            return "delete"
        else:
            return "create"
    @property
    def type(self):
        return self.cls.__name__
    # TODO add a way to get uuid of existing edge

# =======================================
# Fedge (for federated edge) is an emulation of edge

class Fnode():
    """ meta class instanciated when trying to access external node """
    def __init__(self, url):
        self._url = url
    def __eq__(self, other):
        return self.name == other.name
    @property
    def name(self):
        return self._url
    def get_absolute_url(self):
        return self._url
    def asDict(self):
        return {'type': typename(self), 'name': self.name, 'url': self.get_absolute_url()}
    class Meta:
        abstract = True

class Fedge(Edge):
    """ federated edge is an emulation of edge with the kind stored as string (except for Isonym) """
    kind = models.CharField(max_length=32) # might be implemented or not
    def isSymmetric(self):
        try:
            if isinstance(self, Outgoing):
                Ingoing.objects.get(_fromNode=self._toNode, toNode=self.fromNode, kind=self.kind)
            if isinstance(self, Ingoing):
                Outgoing.objects.get(fromNode=self.toNode, _toNode=self._fromNode, kind=self.kind)
            return True
        except Edge.DoesNotExist: # might have to catch type(self).DoesNotExist
            return False
    class Meta:
        abstract = True

class Outgoing(Fedge):
    """ base class for outgoing federated edges """
    _toNode = models.CharField(max_length=256) # url of external node
    @property
    def toNode(self):
        """ simulate the return of a node """
        return Fnode(self._toNode)
    class Meta:
        abstract = True

class Ingoing(Fedge):
    """ base class for ingoing federated edges """
    _fromNode = models.CharField(max_length=256) # url of external node
    @property
    def fromNode(self):
        """ simulate the return of a node """
        return Fnode(self._toNode)
    class Meta:
        abstract = True
