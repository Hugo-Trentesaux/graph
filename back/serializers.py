from marshmallow import Schema, fields, post_load, INCLUDE

from .utils import *
from .models import *

allPublicDict = publicKindDict(DBO) # cache the result
typeof = lambda self, context: typename(self) # alias for typename

def kind(kindName):
    return allPublicDict[kindName]
def getObject(uuid): # get object based on uuid
    return DBO.objects.get(pk=uuid)

class customUUID(fields.UUID):
    def _serialize(self, value, attr, obj, **kwargs):
        return UUIDtoStr(value)
    def _deserialize(self, value, attr, data, **kwargs):
        raise Exception("not suposed to deserialize an uuid")

class DBOSchema(Schema):
    _type = DBO
    type = fields.Function(serialize=typeof, deserialize=kind)
    # this makes a deserializer work for all of it's children whose extra fields are natively serializable
    # for example you can use NodeSchema(unknown=INCLUDE) to deserialize into <wot.Member> even pubkey is given
    # it should work for basically anything since more complex data structures are tied through ForeignKey
    #
    # to serialize, prefer using an adapted serializer to get all the fields
    # you can still use EdgeSchema to serialize <base.Follow> for example
    # since it has no additional field compared to it
    uuid = customUUID(attribute="pk", dump_only=True)
    @post_load
    def toObject(self, data):
        self._type = data.pop('type')
        self.translateData(data) # change data if vocabulary is not good
        return self._type(**data)
    def translateData(self, data): # unchanged
        return data

class NodeSchema(DBOSchema):
    _type = Node
    name = fields.Str()

class EdgeSchema(DBOSchema):
    _type = Edge
    source = fields.Function(serialize=lambda self: self.fromNode.pk, deserialize=getObject)
    target = fields.Function(serialize=lambda self: self.toNode.pk, deserialize=getObject)
    def translateData(self, data): # because names given in serialization are not the same as in python object
        source = data.pop('source')
        target = data.pop('target')
        data.update({'fromNode': source, 'toNode': target})
        return data


# from back.serializers import *
# e = Edge.objects.first()
# s = EdgeSchema()
# j = s.dump(e)
# j.pop('uuid')
# f = s.load(j)

# from back.serializers import *
# from wot.models import *
# n = Member.objects.first()
# s = NodeSchema()
# j = s.dump(n)
# j.pop('uuid')
# m = s.load(j)
