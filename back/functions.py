import networkx as nx
import json
from .models import Node, Edge, GraphDump
from .utils import UUIDtoStr

# networkx documentation: https://networkx.github.io/documentation/stable/index.html

# get all the graph (snapshot)
# because this operation basically consist in retrieveing the whole database,
# it should only be performed in background
# useful to create a dump of the database current state
def getGraphAsDict():
    """ returns the current state of the database as a dict
    can be long if the graph is huge, to be run only at the beginning """
    nodes = Node.objects.all()
    edges = Edge.objects.all()
    # reverse table giving order of a node based on its uuid
    table = { node.pk: i for (i, node) in enumerate(nodes) }
    # data: nodes and edges
    data = {
        # list of nodes
        'nodes': [{ 'name': node.name,
                    'uuid': UUIDtoStr(node.pk),
                    } for node in nodes],
        # list of edges
        'edges': [{ 'uuid': UUIDtoStr(edge.pk),
                    'source': table[edge.fromNode.pk],
                    'target': table[edge.toNode.pk],
                    } for edge in edges],
    }
    return data

# takes a dictionnary representing the graph and applies a drawing algorithm on
# it to get a good start position to present to the user
def layoutGraph(data):
    """ complete the given dictionnary with x and y values
    can be long if graph is huge, to be run only at the beginning """

    G = nx.DiGraph()

    for edge in data['edges']:
        G.add_edge(edge['source'], edge['target'])

    pos = nx.kamada_kawai_layout(G)

    for id, position in pos.items(): # position is a 2 components numpy array
        data['nodes'][id].update({'x': position[0], 'y': position[1]})

    return data

# dumps the given dict into a json string in a new GraphDump object
def dumpGraph(data):
    """ dumps the graph in a dumpGraph object """
    new = GraphDump()
    new.jsonData = json.dumps(data)
    new.save()
    return new

# dumps the current state of the graph in a GraphDump database object
# can be performed regularly (once a day) to backup the state of the graph
# (but not the information related to the objects)
# performs it twice because the last dumpGraph instance will be modified by successful operations
def newGraphDump():
    data = layoutGraph(getGraphAsDict())
    # dumpGraph(data)
    dumpGraph(data)


def loadJsonToNX(data):
    """ returns a nx graph object with json graph structure inside with edge uuid """
    G = nx.DiGraph()
    for edge in data['edges']:
        G.add_edge(int(edge['source']), int(edge['target']), uuid=edge['uuid'])
    return G

# to avoid having to regenerate the graph cache, updates the last exported version.
# as edge's source and target correspond to node reference in list, if one was deleted, we have to re-number
# so the complete cache is rewriten for each modification (~1MB)
# that's why this file has to remain quite minimal. extra features could be asked directy to the database

def getId(objs, uuid): # returns the position of the dict with key "uuid" in the list "objs"
    for (i, obj) in enumerate(objs):
        if obj['uuid'] == uuid:
            return i # only one obj is supposed to have this uuid

def addNode(data, node):
    data['nodes'].append({'name': node.name, 'uuid': UUIDtoStr(node.pk), 'x':0, 'y':0})
def addEdge(data, edge):
    sourceId = getId(data['nodes'], edge.fromNode.pk)
    targetId = getId(data['nodes'], edge.toNode.pk)
    data['edges'].append({'uuid': UUIDtoStr(edge.pk), 'source': sourceId, 'target': targetId})
def addToCache(obj):
    """ updates the last GraphDump json object upon a successful operation """
    gd = GraphDump.objects.last()
    data = json.loads(gd.jsonData)
    if isinstance(obj, Edge):
        addEdge(data, obj)
    elif isinstance(obj, Node):
        addNode(data, obj)
    gd.jsonData = json.dumps(data)
    gd.save()

def delEdge(data, uuid):
    id = getId(data['edges'], uuid)
    data['edges'].pop(id)
def delNode(data, uuid):
    id = getId(data['nodes'], uuid)
    G = loadJsonToNX(data)
    try: # if node is not linked to any edge, it is not loaded in the digraph
        G.remove_node(id)
    except nx.exception.NetworkXError:
        pass # then, no need to remove it
    relabeledEdges = []
    H = nx.relabel.convert_node_labels_to_integers(G) # do the renumbering automatically
    for edge in H.edges: # recreate the edge list without unlinked edges and with good numbers
        relabeledEdges.append({'source': edge[0], 'target': edge[1], 'uuid': H.edges[edge]['uuid']})
    data['nodes'].pop(id) # remove given node
    data['edges'] = relabeledEdges # replace edge list
def delFromCache(obj):
    """ updates the last GraphDump json object upon a successful operation """
    gd = GraphDump.objects.last()
    data = json.loads(gd.jsonData)
    if isinstance(obj, Edge): # easy case
        delEdge(data, obj.pk)
    elif isinstance(obj, Node): # here have to renumber
        delNode(data, obj.pk)
    gd.jsonData = json.dumps(data)
    gd.save()
