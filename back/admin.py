from django.contrib import admin

# Register your models here.

from .models import Node, Edge, GraphDump

admin.site.register(Node)
admin.site.register(Edge)
admin.site.register(GraphDump)
