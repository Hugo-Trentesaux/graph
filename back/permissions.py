from django.core.exceptions import ValidationError
from .utils import typename

# compose several decorators
def composed(*decs):
    def deco(f):
        for dec in reversed(decs):
            f = dec(f)
        return f
    return deco

# check if class is public decorator
def checkPublic(fn):
    def checked(self, **kwargs):
        if not self.isPublic: # only public classes should exist in database
            raise ValidationError("Class is not public (not available to user)")
        return fn(self, **kwargs)
    return checked

# base permission class which DBO inherits from to make sure all the needed functions are defined
class Permission():
    @checkPublic
    def creatable(self, **kwargs):
        return True
    def editable(self, **kwargs):
        return True
    def deletable(self, **kwargs):
        return True

# =====

# name not empty decorator
def notEmpty(fn):
    def notEmpty(self, **kwargs):
        if not self.name:
            raise ValidationError("Empty name not allowed")
        return fn(self, **kwargs)
    return notEmpty

# name not empty class decorator
def notEmptyName(cls):
    cls.creatable = notEmpty(cls.creatable)
    cls.editable = notEmpty(cls.editable)
    return cls

# =====

def no(*args):
    return False
def yes(*args):
    return True

def readOnly(cls):
    cls.creatable = no
    cls.editable = no
    cls.deletable = no
    return cls

# =====

def public(cls):
    cls.isPublic = True
    return cls
