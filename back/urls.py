from django.urls import path
from . import views

app_name = 'back'
urlpatterns = [
    # API GET views
    path('object/create/', lambda r : views.ObjectView(r, None), name='newObject'),
    path('object/<str:uuid>/', views.ObjectView, name='object'), # GET, POST, PATCH, DELETE
    path('local/<str:uuid>/', views.LocalView , name='local'),
    path('graph/', views.GraphView , name='graph'),
    path('meta/', views.MetaView , name='meta'), # returns meta information about graph (node and edge kinds available, version...)
]
