from django.http import JsonResponse, HttpResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import ValidationError

from .models import *
from .utils import *
from .serializers import DBOSchema
from .functions import dumpGraph

allPublicKinds = publicKindDict(DBO)
allSchemaDict = schemaDict(DBOSchema)
def getSchema(cls):
    return firstKeyIn([c for c in cls.__mro__], allSchemaDict)()
nodeKinds = publicKindNames(Node) # TODO replace by a tree
edgeKinds = publicKindNames(Edge)

# error shortcuts
def J400(message):
    return JsonResponse({}, status=400, reason=message)
def csrf_failure(request, reason=""):
    return JsonResponse({}, status=403, reason="unauthorized")

# return graph metadata
def MetaView(request):
    data = {
        # node and edges kinds implementend in local instance
        'nodeKinds': list(nodeKinds),
        'edgeKinds': list(edgeKinds),
        # extra info on the graph itself
        'info': {
            'software version': "0.0"
        }
    }
    return JsonResponse(data)

# call API views
def ObjectView(request, uuid):
    if request.method == 'GET':
        return GET_object(uuid)
    elif request.method == 'POST':
        return POST_object(request)
    elif request.method == 'PATCH':
        return PATCH_object(request, uuid)
    elif request.method == 'DELETE':
        return DELETE_object(request, uuid)

# return total graph stored in cache
def GraphView(request):
    gd = GraphDump.objects.last()
    if gd is None:
        gd = dumpGraph({'nodes':[], 'edges':[]})
        print(gd)
    return HttpResponse(gd.jsonData, content_type="application/json")

# return a local view of a node based on it's uuid
def LocalView(request, uuid):
    """ get a local graph centered around the given node """
    try:
        node = Node.objects.get(pk=uuid)
    except Node.DoesNotExist:
        return JsonResponse(status=404, reason="Node does not exist")

    nodes = [{'name': node.name, 'group': 0}]
    edges = []
    table = {node.name: 0}

    depth = 1
    # TODO use node.propagate to get levels of propagation
    for i, edge in enumerate(node.edgeFrom.all()): # browse node edges
        nodes.append({'name': edge.toNode.name, 'group': 1})
        edges.append({'source':0, 'target': i+1, 'value': 1})

    data = {'nodes': nodes, 'edges': edges}
    return JsonResponse(data)


# ============================== edition =======================================
import json
from .functions import addToCache, delFromCache
from copy import deepcopy

# ================== GET, POST, PATCH, DELETE views ==================

# get an existing object
def GET_object(uuid):
    try:
        obj = DBO.objects.get(pk=uuid)
    except DBO.DoesNotExist:
        return JsonResponse({}, status=404, reason="Object not found")
    s = getSchema(type(obj))
    return JsonResponse(s.dump(obj), reason="Object found")

# creates a new object of the given class and fills it with the given properties, raise error if malformed
def POST_object(request):
    """ creates the object given in request """
    dic = json.loads(request.body)
    try: # check if type is given
        otype = dic["type"]
    except KeyError:
        raise ValidationError("Type not specified")
    try: # check if type is implemented
        cls = allPublicKinds[otype]
    except KeyError:
        raise ValidationError(f"{otype} not public or not implemented")
    s = getSchema(cls) # get best existing schema
    new = s.load(dic) # deserialize to an instance (type(new)==dic['type'])
    try: # save the new instance
        new.creatable(user=request.user) # check permission
        new.save()
    except ValidationError as e:
        return J400(e.args[0])
    # ===== SUCCESS block =====
    # updates the cache by adding the object TODO replace by signal
    addToCache(new)
    # returns the success message and new instance (with uuid)
    return JsonResponse(s.dump(new), reason="Object saved successfully")

# updates and existing object to change it's fields content based on dictionnary
# for now it doesn't change the graph cache, only information related to an object
def PATCH_object(request, uuid):
    """ updates the node given in request """
    try: # tries to get the object
        instance = DBO.objects.get(pk=uuid)
    except DBO.DoesNotExist:
        return JsonResponse({}, status=404, reason="Object not found")
    for key, value in json.loads(request.body).items():
        setattr(instance, key, value) # maybe filter the keys ! TODO partial deserializer
    try:
        instance.editable(user=request.user)
        instance.save()
    except ValidationError as e:
        return JsonResponse({}, status=400, reason=e.args[0])
    # ===== SUCCESS block =====
    s = getSchema(type(instance)) # get best existing schema
    return JsonResponse(s.dump(instance), reason="Object updated successfully")

# deletes an object based on it's class and uuid
def DELETE_object(request, uuid):
    """ deletes the object referenced in request """
    try: # tries to get the object
        instance = DBO.objects.get(pk=uuid)
    except DBO.DoesNotExist:
        return JsonResponse({}, status=404, reason="Object not found")
    sibling = deepcopy(instance) # because pk is lost when deleting TODO replace by post_delete signal
    try: # delete the instance
        instance.deletable(user=request.user)
        instance.delete()
    except ValidationError as e:
        return J400(str(e))
    # ===== SUCCESS block =====
    delFromCache(sibling)
    return JsonResponse({}, reason="Object deleted successfully")
