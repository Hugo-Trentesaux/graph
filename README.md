*il se trouve que j'ai implémenté une sorte de [Gremlin](https://en.wikipedia.org/wiki/Gremlin_(programming_language)) avec un frontend similaire à [Graphexp](https://github.com/bricaud/graphexp)*

# Graph

Le projet *graph* (nom à trouver) consiste à fournir une architecture de base extensible et fédéré pour tout réseau pouvant se modéliser simplement par un graphe. Il doit également permettre une interaction de l'utilisateur représenté par un nœud avec le graphe.

Les éléments clés sont deux modèles python : **Node** (nœud) et **Edge** (arête). Un Node peut représenter n'importe quel objet et une Edge relie de manière directionnelle deux Node. À partir de là, beaucoup de choses son possibles.

Des exemples d'extensions son décrits par la suite. Ils peuvent prendre la forme de plusieurs instances fédérées via un protocole permettant des liens entre deux nœuds situés sur des instances différentes.

### Installation

Lire les [instructions d'installation](./doc/install.md).

## Exemples d'utilisation

Le programme *graph* pourrait être adapté aux applications suivantes. Les **nœuds** et les **liens** sont signalés en italique. Les liaisons directionnelles sont notées par des flèches (→) et les liaisons symétriques, ou bidirectionnelles sont notées par des flèches doubles (↔).

### Monnaie libre

La monnaie libre ğ1 repose sur une toile de confiance, c'est à dire un graphe dans lequel les **nœuds** sont des *individus* et les **liens** sont des *relations de confiance directionnelle* (individu → individu). On peut visualiser ce graphe sur le site [wotmap.duniter.io][1].

L'exploration de ce graphe permet de découvrir des communautés géographiques, un architecture d'organisation...

Le projet *graph*, en dupliquant cette toile de confiance et en l'exposant via une API simple d’utilisation devrait permettre d'interconnecter cette toile avec d'autres réseaux décrit ci-dessous.

[[lire plus]](./doc/monnaie-libre.md)

### Publications scientifiques

[Researchgate.net][2] est un réseau dans lequel les **nœuds** sont des *chercheurs*, des *laboratoires*, des *publications scientifiques*, des *organismes de financement* et les **liens** sont des *citations* (article → article), des *auteurs* (article ↔ chercheur), des *reviewers* (article ↔ chercheur), des *lectures* (chercheur → article), des *appartenances* (chercheur ↔ labo), des *financements* (organisme → nœud)

Ce réseau est complexe et sa compréhension pourrait mener à des conclusions intéressantes, faire ressortir des articles, des aberrations de financement...

### Contenu créatif

De nombreux créateurs proposent leur contenu sur internet, et peinent à trouver du public. Exposer les *créateurs*, *vidéos*, *articles*, *photos*, *dessins*, *livres*, *films*, sur un tel graphe permettrait de faire ressortir les contenus autrement que par les algorithmes développés par les plateformes, qui tendent à favoriser les gros créateurs et à écraser les petits.

De la part de l'*utilisateur*, cela permettrait de naviguer dans un réseau créatif et d'identifier des points d'intérêt de ses créateurs préférés, de ses amis...

La diversité du contenu créatif pourrait mener à la création de plusieurs instances spécialisées. Par exemple le cinéma avec les *acteurs*, les *réalisateurs*, les *films*. Le site [lacinetek.com][3] propose ainsi de visionner les films sélectionnés par des réalisateurs, constituant un réseau de recommandation et d'inspiration.

Ces instances pourraient évoluer indépendamment, mais rester reliées par un protocole de fédération.

### Associations

L'idée à l'origine de ce projet était de mettre en lien des association présentes en ligne via des plateformes comme [associations.gouv.fr][4-a] et [francebenevolat.org][4-b] pour coopérer sur des enjeux de société *a priori* très distants. On pourrait ainsi trouver des bénévoles communs aux causes environnementales et sociétales et les unir à travers un projet commun et cohérent.

Ce principe peut également être étendu à la recherche d'emploi dans des entreprises...

### Réseaux sociaux

Les réseaux sociaux généralistes profitent en général de leur connaissance du graphe pour une fin principale : proposer de la publicité ciblée. Lorsque l'on consulte Facebook, par exemple, il ne nous laisse consulter que les relations d'ordre 1, c'est à dire les amis en commun entre votre profil et un autre.

Avec les données rendues publiques sur Facebook, il serait également possible d'accéder aux relations d'ordre supérieures : quel chemin minimal vous mène à telle personne ? quel est le facteur de clustering de votre cercle de connaissances ? Cloner les données publiques de Facebook ainsi que de Twitter, Instagram ou LinkedIn et les interconnecter permettrait une utilisation plus intéressantes des caractéristiques de ce graphe.

## Organisation du programme

Le programme est organisé en plusieurs modules : *back*, *front*, et *base*. Pour l'instant, la partie *back* gère les modèles et la base de données et la rend disponible à travers une API. La partie *front* propose une interface d'administration visuelle généraliste (complémentaire au site admin de django). La partie base implémente des modèles de base héritant de Node et Edge et une interface utilisateur basique. Elle doit servir d'exemple pour développer d'autres modules.

![interface visuelle d'administration](./doc/img/graph.png)

> Exemple de la fenêtre d'administration.

### développement

Pour en lire plus sur les fonctionnalités envisagées.

- [général](./doc/général.md)
- [jeux](./doc/jeux.md)
- [overview](./doc/overview.md)





























[1]: https://wotmap.duniter.io/
[2]: https://www.researchgate.net/
[3]: https://www.lacinetek.com/fr/
[4-a]: https://www.associations.gouv.fr/les-associations-en-france.html
[4-b]: https://www.francebenevolat.org/associations
