from django.db import models
from back.models import Node, Edge
from back.permissions import readOnly, public

@readOnly
@public
class Member(Node):
    pubkey = models.CharField(max_length=64) # represents the pubkey

@readOnly
@public
class Certification(Edge):
    AllowedFromTo = [ (Member, Member) ]
