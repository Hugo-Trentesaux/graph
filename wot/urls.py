from django.urls import path
from . import views

app_name = 'wot'
urlpatterns = [
    path('api/graph/', views.GraphView , name='graph'),
    # path('view/', views.Wotmap , name='wotmap'),
]
