from django.http import JsonResponse
import json

from .models import Member, Certification

# specific API views

def GraphView(request):
    """ returns the whole graph with minimal information """
    nodes = Member.objects.all()
    edges = Certification.objects.all()

    # reverse table giving the position of a node in the list based on its uuid
    table = {node.pk: i for (i, node) in enumerate(nodes)}

    data = {
        # list of node names and uuid
        'nodes': [{ 'name': node.name,
                    'uuid': node.pk,
                    } for node in nodes],
        # list of edges based on node order
        'edges': [{ 'source': table[edge.fromNode.pk],
                    'target': table[edge.toNode.pk],
                    'uuid': edge.pk,
                    # 'value': 1,
                    } for edge in edges],
        # extra info on the graph itself
        'info': {   'number of nodes': len(nodes),
                    'number of edges': len(edges),
                    }
    }
    return JsonResponse(data)


def upload(filepath):
    """ upload the json file to the DB (1500 point, 15000 links ~= 2 min)"""
    with open(filepath) as fid:
        content = fid.read()
        data = json.loads(content)

    nodes = []

    for node in data['nodes']:
        new = Member(   name=node['label'],
                        pubkey=node['attributes']['pubkey'] )
        nodes.append(new)

    for node in nodes:
        try:
            node.save()
        except:
            print(node)

    edges = []

    for edge in data['edges']: # edges start to count from 1
        fromNodepubkey = nodes[int(edge['source'])-1].pubkey
        toNodepubkey = nodes[int(edge['target'])-1].pubkey
        new = Certification( fromNode=Member.objects.get(pubkey=fromNodepubkey),
                             toNode=Member.objects.get(pubkey=toNodepubkey) )
        edges.append(new)

    for edge in edges:
        try:
            edge.save()
        except:
            print(edge)


# specific display views
