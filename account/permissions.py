from django.core.exceptions import ValidationError
from back.utils import typename

# check if user is provided decorator combination
def userProvided_decorator(action, fn):
    def userProvided(self, **kwargs):
        try: # must tell the user to save/delete the object
            user = kwargs['user']
        except KeyError: # user not provided in kwargs
            raise ValidationError(f"Please provide an user to {action} this")
        return fn(self, **kwargs)
    return userProvided

# check if user is allowed to perform this (check user existance first)
def editable_decorator(action, fn):
    def editable(self, **kwargs):
        user = kwargs['user']
        if not self.isEditableBy(user):
            if action=="create":
                raise ValidationError(f"User {user} not allowed to create {typename(self)}")
            if action=="edit":
                raise ValidationError(f"User {user} not allowed to edit {typename(self)}: {self}")
            if action=="delete":
                raise ValidationError(f"User {user} not allowed to delete {self}")
        return fn(self, **kwargs)
    return editable

# combine userProvided_decorator and editableBy_decorator for given action
def editable(action):
    return lambda fn : userProvided_decorator(action, editable_decorator(action, fn))

def isEditableBy(self, user):
    try:
        return user.account.canEdit(self)
    except AttributeError:
        return False # user has no account TODO write true authentication method

# add basic checks on a class
def userRequired(cls):
    # decorates the action control functions
    cls.creatable = editable("create")(cls.creatable)
    cls.editable = editable("update")(cls.editable)
    cls.deletable = editable("delete")(cls.deletable)
    # creates the isEditableBy function
    cls.isEditableBy = isEditableBy
    return cls
