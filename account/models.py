from django.db import models
from django.contrib.auth.models import User

from back.models import *
from back.permissions import public

@public
class Account(Node):
    """ account is a general node that can control several other nodes
    it can have several auth methods """
    active = models.ForeignKey('Control', null=True, blank=True, on_delete=models.SET_NULL, related_name="+") # defines which node this account is currently controlling
    def node(self):
        if not self.active: # if no active control
            return None
        return self.active.toNode

class AuthMethod(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="authmethods")
    class Meta:
        abstract = True

class DjangoAM(AuthMethod):
    """ account class relying on django's user class """
    djangoUser = models.OneToOneField(User, related_name='authmethod', on_delete=models.CASCADE)

@public
class Control(Edge):
    Symmetrical = True
    AllowedFromTo = { (Account, Node) }
